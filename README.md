# Ecosystem

This project is used to up all the project and dependencies. Use `make` to run actions:

```
    Make actions:


    doc: 	read this documentation
    up: 	up all the services
    build: 	build all services
    api: 	clone the api project (you can make a simbolic link to api service too)
    frontend: 	clone the frontend project (you can make a simbolic link to frontend service too)
```
