.SILENT: doc
.PHONY: doc up build

COMPOSE_HTTP_TIMEOUT:=120

# doc: read this documentation
doc:
	echo ''
	echo '    Make actions:'
	echo ''
	grep -P '^\w+:' -B 1 Makefile | grep -v -P '^\w+:' | grep -v -P '^--$$' | sed 's,# ,    ,' | sed 's,: ,: \t,'
	echo ''

# up: up all the services
up: api frontend
	docker-compose down
	docker-compose up -V

# build: build all services
build: api frontend
	docker-compose build

# api: clone the api project (you can make a simbolic link to api service too)
api:
	git clone https://gitlab.com/mg-service-care/api.git

# frontend: clone the frontend project (you can make a simbolic link to frontend service too)
frontend:
	git clone https://gitlab.com/mg-service-care/frontend.git
